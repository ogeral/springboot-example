package application

import org.springframework.context.i18n.LocaleContextHolder

import javax.ws.rs.core.Response

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE
import static javax.ws.rs.core.Response.Status.*

/**
 * Created by corozco on 6/11/15.
 */
class ApiResponse {
//TODO REVISAR FLUJOS FUNCIONALIDAD DE LAS EXCEPCIONES
  static Response ok(Object entity) {
    Response.status(OK)
      .entity(entity)
      .type(APPLICATION_JSON_TYPE)
      .language(LocaleContextHolder.getLocale())
      .build()
  }

  static Response badRequest(Object entity) {
    Response.status(BAD_REQUEST)
      .entity(entity)
      .type(APPLICATION_JSON_TYPE)
      .language(LocaleContextHolder.getLocale())
      .build()
  }

  static Response notContent(Object entity) {
    Response.status(OK)
      .entity(entity)
      .type(APPLICATION_JSON_TYPE)
      .language(LocaleContextHolder.getLocale())
      .build()
  }

  static Response notFound(Object entity) {
    Response.status(NOT_FOUND)
      .entity(entity)
      .type(APPLICATION_JSON_TYPE)
      .language(LocaleContextHolder.getLocale())
      .build()
  }

  static Response internalError(Object entity) {
    Response.status(INTERNAL_SERVER_ERROR)
      .entity(entity)
      .type(APPLICATION_JSON_TYPE)
      .language(LocaleContextHolder.getLocale())
      .build()
  }
}
