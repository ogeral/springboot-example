package application

import dto.v1.HomeDto
import jaxrs.v1.HomeResource
import org.springframework.stereotype.Component

import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.Response

import static javax.ws.rs.core.MediaType.APPLICATION_JSON

import static application.ApiResponse.ok

/**
 * Created by corozco on 7/14/15.
 */
@Path("/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@Component
class HomeController implements HomeResource {
  @Override
  @GET
  Response getHome() {
    ok(new HomeDto(name: "Home example", version: "0.1"))
  }
}
