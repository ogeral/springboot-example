package application

import dto.v1.BadRequestDto
import dto.v1.ErrorDto
import dto.v1.SaymynameDto
import jaxrs.v1.SaymynameResource
import org.springframework.stereotype.Component

import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Response

import static application.ApiResponse.badRequest
import static application.ApiResponse.ok
import static javax.ws.rs.core.MediaType.APPLICATION_JSON

/**
 * Created by corozco on 7/14/15.
 */
@Path("/saymyname")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@Component
class SaymynameController implements SaymynameResource {
  @Override
  @GET
  Response getSaymyname(
    @QueryParam("name") String name) {
    if (name) {
      ok(new SaymynameDto(success: true, name: name))
    } else {
      badRequest(new BadRequestDto(success: false, errors: [new ErrorDto(object: "/saymyname", rejected_value: name, field: "name", message: "El campo es obligatorio")]))
    }
  }
}
