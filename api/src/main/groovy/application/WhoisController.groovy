package application

import dto.v1.WhoisDto
import jaxrs.v1.WhoisResource
import org.springframework.stereotype.Component

import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.Response

import static javax.ws.rs.core.MediaType.APPLICATION_JSON

import static application.ApiResponse.ok

/**
 * Created by corozco on 7/14/15.
 */
@Path("/whois")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@Component
class WhoisController implements WhoisResource {
  @Override
  @GET
  Response getWhois() {
    ok(new WhoisDto(success: true, contact: "ogeral@gmail.com", author: "Antonio Medina"))
  }
}
