package conf

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import groovy.util.logging.Log4j
import org.glassfish.jersey.servlet.ServletContainer
import org.glassfish.jersey.servlet.ServletProperties
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.context.embedded.ServletRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

/**
 * Created by corozco on 7/14/15.
 */
@Configuration
@EnableAutoConfiguration
@Log4j
class Application {
  static void main(String[] args) throws Exception {
    def ctx = SpringApplication.run(Application, args)
    if (log.debugEnabled) {
      ctx.beanDefinitionNames.each { log.debug it }
    }
  }

  @Bean
  ServletRegistrationBean jerseyServlet() {
    ServletRegistrationBean registration = new ServletRegistrationBean(new ServletContainer(), '/v1/*')
    registration.addInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, JerseyConfigV1.name)
    registration
  }

  @Bean
  @Primary
  Gson getGson() {
    new GsonBuilder()
      .setPrettyPrinting()
      .setVersion(1.0)
      .registerTypeAdapter(Long.class, new TypeAdapter<Number>() {
      @Override
      public Number read(JsonReader reader) throws IOException {
        if (reader.peek() == JsonToken.NULL) {
          reader.nextNull()
          return null
        }
        try {
          String result = reader.nextString()
          if (result.isEmpty() || result.isAllWhitespace()) {
            return null;
          } else if (result.isLong()) {
            return Long.parseLong(result)
          } else if (result.isDouble()) {
            return Double.parseDouble(result)
          }
        } catch (NumberFormatException e) {
          throw new JsonSyntaxException(e);
        }
      }

      @Override
      public void write(JsonWriter writer, Number value) throws IOException {
        if (value == null) {
          writer.nullValue();
          return;
        }
        writer.value(value);
      }
    }).create()
  }

  @Bean
  Gson gsonNullify() {
    new GsonBuilder()
      .setPrettyPrinting()
      .setVersion(1.0)
      .serializeNulls()
      .create()
  }
}
