package conf

import com.google.gson.Gson
import org.springframework.beans.factory.annotation.Autowired

import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.ext.MessageBodyReader
import javax.ws.rs.ext.MessageBodyWriter
import java.lang.annotation.Annotation
import java.lang.reflect.Type

/**
 * Created by corozco on 6/11/15.
 */
class GsonProvider <T> implements MessageBodyReader<T>, MessageBodyWriter<T> {

  @Autowired
  Gson gson

  @Override
  long getSize( T t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType  ) {
    -1
  }

  @Override
  boolean isWriteable( Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType ) {
    true
  }

  @Override
  void writeTo( T object,
                Class<?> type,
                Type genericType,
                Annotation[] annotations,
                MediaType mediaType,
                MultivaluedMap<String, Object> httpHeaders,
                OutputStream entityStream ) throws IOException, WebApplicationException
  {
    def printWriter = new PrintWriter( entityStream )
    String json = gson.toJson( object )
    printWriter.write( json )
    printWriter.flush()
  }

  @Override
  boolean isReadable( Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType ) {
    true
  }

  @Override
  T readFrom( Class<T> type,
              Type gnericType,
              Annotation[] annotations,
              MediaType mediaType,
              MultivaluedMap<String, String> httpHeaders,
              InputStream entityStream ) throws IOException, WebApplicationException
  {
    InputStreamReader reader = new InputStreamReader( entityStream, 'UTF-8' )
    gson.fromJson( reader, type )
  }
}