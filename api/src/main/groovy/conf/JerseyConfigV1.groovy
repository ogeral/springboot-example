package conf

import org.glassfish.jersey.server.ResourceConfig
import org.glassfish.jersey.server.spring.scope.RequestContextFilter

/**
 * Created by corozco on 6/11/15.
 */
class JerseyConfigV1 extends ResourceConfig {
  JerseyConfigV1() {
    register(RequestContextFilter)
    register(GsonProvider)
    packages('application')
  }
}
