package services

import org.springframework.stereotype.Service

/**
 * Created by ogeral on 13/07/15.
 */

@Service
class ExampleService {
    public String defaultValue = "some public variable"

    public Map exampleData(){
        [
                "some" : "nothing to say",
                "date" : "today",
                "dval" : defaultValue
        ]
    }
}
