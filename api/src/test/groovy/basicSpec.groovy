package services

import spock.lang.Specification

/**
 * Created by ogeral on 13/07/15.
 */
class basicSpec extends Specification{
    def "Basic spoc example"(){
        when:
            ExampleService exampleService = new ExampleService()
            Map data = exampleService.exampleData()
        then:
            data.size() == 3
    }
}
