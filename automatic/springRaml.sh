#!/usr/bin/env bash
clear

echo ""
echo ""
echo "Springboot api rest applicacion"
echo "-------------------------------"

rm -rf springRaml.tgz
wget https://bitbucket.org/ogeral/springboot-example/src/c93ac76ab3fa18ab717a0622bef9b918c0f179ab/automatic/springRaml.tgz?at=raml -O springRaml.tgz

if [ ! -f "springRaml.tgz" ]; then
    echo "Error: Cant download source"
    exit
fi

TestMode="true"

APAction="$1" #Operation
APName="$2" #Application Name
APProyectName="$3"

if [ "$TestMode" == "true" ]; then
	#Java application required
	DOWNLOADJAVAFILE="jre-7u71-linux-i586.tar.gz"
	DOWNLOADJAVAPATH="http://download.oracle.com/otn-pub/java/jdk/7u71-b14/$DOWNLOADJAVAFILE"
	DOWNLOADJAVAVERSION="7u71"
	DOWNLOADJAVAJDK="jre1.7.0_71"
	
else
	#Java application required
	DOWNLOADJAVAFILE="jdk-7u7-linux-x64.tar.gz"
	DOWNLOADJAVAPATH="http://download.oracle.com/otn-pub/java/jdk/7u7-b10/$DOWNLOADJAVAFILE"
	DOWNLOADJAVAVERSION="7u72"
	DOWNLOADJAVAJDK="jdk1.7.0_07"
fi

export JAVA_HOME=/usr/local/bin/$DOWNLOADJAVAJDK

PIDNAME="applicationBlock.pid"
SERVERINSTANCETYPE="gz.light"

function updateProyect(){
	sed -e "s/$2/$3/g" "$1" > "$1.tmp"
	mv  "$1.tmp" "$1"
}
function meliHosts {
	updateProyect "$2/makefile" "#export http_proxy=" "export http_proxy="
	updateProyect "$2/makefile" "#export https_proxy=" "export https_proxy="
	updateProyect "$2/makefile" "#export http_proxy=" "export http_proxy="
	updateProyect "$2/makefile" "#export https_proxy=" "export https_proxy="
	updateProyect "$2/makefile" "jdk-7u7-linux-x64.tar.gz" "$DOWNLOADJAVAJDK"
	updateProyect "$2/makefile" "7u72" "$DOWNLOADJAVAVERSION"
	updateProyect "$2/makefile" "7u7-b10" "7u7-b10"
	updateProyect "$2/makefile" "jdk1.7.0_07" "$DOWNLOADJAVAJDK"


	updateProyect "$2/$2/makefile" "#export http_proxy=" "export http_proxy="
	updateProyect "$2/$2/makefile" "#export https_proxy=" "export https_proxy="
	updateProyect "$2/$2/makefile" "jdk-7u7-linux-x64.tar.gz" "$DOWNLOADJAVAJDK"
	updateProyect "$2/$2/makefile" "7u72" "$DOWNLOADJAVAVERSION"
	updateProyect "$2/$2/makefile" "7u7-b10" "7u7-b10"
	updateProyect "$2/$2/makefile" "jdk1.7.0_07" "$DOWNLOADJAVAJDK"

	updateProyect "$2/$2/meli/bin/install.sh" "#export http_proxy=" "export http_proxy="
	updateProyect "$2/$2/meli/bin/install.sh" "#export https_proxy=" "export https_proxy="
	updateProyect "$2/$2/meli/bin/install.sh" "jdk-7u7-linux-x64.tar.gz" "$DOWNLOADJAVAJDK"
	updateProyect "$2/$2/meli/bin/install.sh" "7u72" "$DOWNLOADJAVAVERSION"
	updateProyect "$2/$2/meli/bin/install.sh" "7u7-b10" "7u7-b10"
	updateProyect "$2/$2/meli/bin/install.sh" "jdk1.7.0_07" "$DOWNLOADJAVAJDK"
	
	updateProyect "$2/$2/meli/bin/config.sh" "jdk1.7.0_07" "$DOWNLOADJAVAJDK"
	updateProyect "$2/$2/meli/bin/startup.sh" "jdk1.7.0_07" "$DOWNLOADJAVAJDK"
	
	updateProyect "$2/$2/makefile" "gradle bootRepackage" "gradle -Dhttp.proxyHost=proxy.meliglobal.com -Dhttp.proxyPort=80 -Dhttps.proxyHost=proxy.meliglobal.com -Dhttps.proxyPort=80 bootRepackage"

	case "$1" in 
		"on")
			echo "Use MeliCloud hosts"

			;;
		"off")
			echo "Remove MeliCloud hosts"
			updateProyect "$2/makefile" "export http_proxy=" "#export http_proxy="
			updateProyect "$2/makefile" "export https_proxy=" "#export https_proxy="
			updateProyect "$2/$2/makefile" "export http_proxy=" "#export http_proxy="
			updateProyect "$2/$2/makefile" "export https_proxy=" "#export https_proxy="
			updateProyect "$2/$2/meli/bin/install.sh" "export http_proxy=" "#export http_proxy="
			updateProyect "$2/$2/meli/bin/install.sh" "export https_proxy=" "#export https_proxy="

			echo "$2/$2/makefile"
			updateProyect "$2/$2/makefile" "gradle -Dhttp.proxyHost=proxy.meliglobal.com -Dhttp.proxyPort=80 -Dhttps.proxyHost=proxy.meliglobal.com -Dhttps.proxyPort=80 bootRepackage" "gradle bootRepackage"
			;;
	esac

}
function initApp {
	echo 
	echo "Prepare for $1"
	#rm -rf $1
	#tar -zxvf springRaml.tgz > /dev/null
	tar -zxvf springRaml.tgz
	mv springRaml $1
	if [ -d $1 ]; then
		updateProyect "$1/settings.gradle" "usersapplication" "$1"
		updateProyect "$1/settings.gradle" "users_migration" "$2"
		mv $1/usersapplication $1/$1
		updateProyect "$1/$1/build.gradle" "userapplication" "$1"
		updateProyect "$1/raml/build.gradle" "usersapplication" "$1"
		updateProyect "$1/raml/package.json" "classifieds-springboot_example" "$2"
		updateProyect "$1/raml/Gulpfile.js" "usersapplication" "$1"
		updateProyect "$1/api-client/build.gradle" "usersapplication" "$1"

		#Meli deploy definitions
		updateProyect "$1/makefile" "webserver" "$1"
		updateProyect "$1/$1/makefile" "webserver" "$1"
		updateProyect "$1/$1/makefile" "finalApplication.jar" "$1-1.jar"
		updateProyect "$1/$1/meli/bin/config.sh" "finalApplication.jar" "$1-1.jar"
		updateProyect "$1/$1/meli/bin/config.sh" "transm3.pid" "$PIDNAME"
		updateProyect "$1/$1/meli/bin/flavor.sh" "gz.light" "$SERVERINSTANCETYPE"

		#Meli test at local
		if [ "$TestMode" == "true" ]; then
			meliHosts "off" $1 $2

		else
			meliHosts "on" $1 $2
		fi;	
	else
		echo ""
		echo "ERROR: Something is wrong, we cant uncompress file"	
		echo ""
		exit
	fi;

}
function buildApp {
	source ~/.gvm/bin/gvm-init.sh
	echo "Build application"
	cd $1
	cd raml
	echo $(pwd)
	gradle clean build
	cd ..
	cd $1
	gradle clean build
	cd ..
}
function installApp {
	echo "Java revision"
	jversionReview=$(java -version)
	#echo $jversionReview
	if [ "$jversionReview" == "" ]; then
		if [ ! -d /usr/local/bin/$DOWNLOADJAVAJDK ]; then
			if [ ! -f $DOWNLOADJAVAFILE ]; then
				wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "$DOWNLOADJAVAPATH"
				echo "Java $DOWNLOADJAVAVERSION downloaded";
			fi 
			tar -xvf $DOWNLOADJAVAFILE; 
		 sudo mv $DOWNLOADJAVAJDK "/usr/local/bin/$DOWNLOADJAVAJDK"

		fi
	fi

	cd $1/raml
	echo "Install raml dependencies"
	if [ ! -f ~/.nvm/nvm.sh ]; then
		wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.25.4/install.sh | bash
	fi;
	. ~/.nvm/nvm.sh
	nvm install v0.10.32
	npm install

	#rm -rf ~/.gvm
	if [ ! -f ~/.gvm/bin/gvm-init.sh ]; then
		echo "Intalling gradle"
		curl -s get.gvmtool.net | bash
	fi
	source ~/.gvm/bin/gvm-init.sh
	gvm install gradle 2.2.1

	cd ..
	cd ..
}


if [ "$APAction" == "" ]; then
	echo "Require action"
	exit
fi;
if [ "$APName" == "" ]; then
	echo "Require application name (no spaces)"
	exit
fi;
if [ "$APProyectName" == "" ]; then
	APProyectName="$APName"
fi;


case "$APAction" in
	'init')
			echo "Init application"
			initApp $APName $APProyectName
			installApp $APName
			buildApp $APName
			;;
	'hostsOn')
			echo "Setting hosts "
			meliHosts "on" $APName $APProyectName 
			;;
	'hostsOff')
			echo "Setting hosts "
			meliHosts "off" $APName $APProyectName 
			;;
	'build')
			buildApp $APName
			;;
	'run')
			cd $APName
			echo pwd
			$JAVA_HOME/bin/java -jar "${APName}"/build/libs/"${APName}-1.jar"
			;;
	'install')
			echo "Install dependencies"
			installApp $APName
			;;
	*)
			echo "Springboot examples"
			echo " init fileName ProyectName"
			;;
esac

echo 
echo "-------------------------------"
echo

#tar -zxvf springRaml.tgz


#file /sbin/init
#uname -a

