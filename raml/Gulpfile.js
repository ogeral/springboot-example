var gulp = require('gulp');
var raml = require('gulp-raml');
var gutil = require('gulp-util');

var raml2html = require('gulp-raml2html');

var raml2code = require("raml2code");
var genPojos = require("raml2code-groovy-pojo");
var genJaxRs = require("raml2code-jaxrs-interfaces");
var genRetrofitClient = require("raml2code-retrofit");
var genJsClient = require("raml2code-js-client-mulesoft");

var packagePojo = "dto";
var packageClient = "client";
var packageJaxRs = "jaxrs";

gulp.task('raml', function () {
  gulp.src('src/index.raml')
    .pipe(raml())
    .on('error', gutil.log)
    .pipe(raml.reporter('default'))
    .pipe(raml.reporter('fail'))
    .pipe(gulp.dest('build'));0
11});

gulp.task("genPojos", ['raml'], function () {
  gulp.src('src/index.raml')
    .pipe(raml2code({generator: genPojos, extra: {package: packagePojo}}))
    .on('error', gutil.log)
    .pipe(gulp.dest('../api-dto/src/main/groovy/dto'));
});

gulp.task("genClient", ['raml'], function () {
  gulp.src('./src/index.raml')
    .pipe(raml2code({generator: genRetrofitClient, extra: {package: packageClient, importPojos: packagePojo}}))
    .on('error', gutil.log)
    .pipe(gulp.dest('../api-client/src/main/groovy/client'));
});

gulp.task("genJaxRs", ['raml'], function () {
  gulp.src('./src/index.raml')
    .pipe(raml2code({generator: genJaxRs, extra: {package: packageJaxRs, importPojos: packagePojo}}))
    .pipe(gulp.dest('../api/src/main/groovy/jaxrs'));
});

gulp.task('build', ['raml', 'genPojos', 'genClient', 'genJaxRs']);