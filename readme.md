## SpringBoot with Raml Basic Example
An SpringBoot basic structure for develop with spock specs.
- Jersey
- RAML

## 1.-Generate contracts with raml
### Generate api version resources
### Generate pojos
### Generate api client
- cd raml
- npm install
- gradle clean build

## 2.-Build application
Compile and make executable file 
- cd api
- gradle clean test build

## 3.-Execute application
- java -jar api/build/libs/springboot-example-1.jar

## Lisent at 
- http://localhost:8080

## Example links
- http://localhost:8080/v1/
- http://localhost:8080/v1/whois
- http://localhost:8080/v1/saymyname?name=pancho

## Aditional notes
- For install npm check
- https://github.com/creationix/nvm#install-script

- For activate just run
- . ~/.nvm/nvm.sh
- nvm install v0.10.32

## springRaml script 
### Allows you to
- Automatic create a springboot raml project with all dependencies
- Includes Meli build basic elements
- Local test configuration

### How to
- Download automatic/springRaml.sh file
- Set execution grants

### Initialize project
- ./springRaml.sh init applicationFolder applicationName
- ./springRaml.sh init springapp springexample

### Aditionals
You can use hosts and server elements or use basic elements, this options can execute at anytime and do not modify the application changes
- ./springRaml.sh hostsOn applicationFolder applicationName
- ./springRaml.sh hostsOff applicationFolder applicationName
- ./springRaml.sh build applicationFolder applicationName
- ./springRaml.sh run applicationFolder applicationName